import tornado.ioloop
import tornado.web
import os, sys
import serial
import time
import glob


APPLICATION_PATH    = os.path.abspath(os.path.dirname(sys.argv[0]))
COMPORT             = ''
HTTP_PORT           = 8085

if (os.name=='nt'):
    COMPORT = 'COM4'
elif (os.name=='posix'):
    COMPORT = '/dev/ttyACM0'

ser = serial.Serial(COMPORT)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.redirect('/www/index.html')

class RestAPIURLHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")

    def post(self, event=None):
        return self.handle(event)

    def get(self, event=None):
        return self.handle(event)

    def handle(self, cmd=None):
        result = {'result': 'error'}
        print cmd
        write_to_serial(cmd)
        self.write(cmd)

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r'/cmd/(.*)', RestAPIURLHandler),
        (r'/www/(.*)', tornado.web.StaticFileHandler, {'path': '%s' % ( os.path.join(APPLICATION_PATH, 'www')) }),
    ])

def write_to_serial(data):
    data = str(data)
    print "Received:", data
    for i in range(0, len(data)):
        ser.write(data[i:i + 1])
        time.sleep(0.1)

def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

if __name__ == "__main__":
    app = make_app()
    app.listen(HTTP_PORT)
    print "Listening on %s" % HTTP_PORT
    list_of_ports = serial_ports()
    print list_of_ports
    tornado.ioloop.IOLoop.current().start()